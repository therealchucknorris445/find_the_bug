
#This is the Streamlit (multipage) class which will allow us to utilize the Streamlit API & create a web app with multiple pages.

import streamlit as st
from pathlib import Path


class MultiPage: 

    def __init__(self) -> None:
        self.pages = []
    
    def add_page(self, title, func) -> None: 
        """Class Method to Add pages to the project

        Args:
            title ([str]): The title of page which we are adding to the list of apps 
            
            func: Python function to render this page in Streamlit
        """

        self.pages.append(
            {
                "title": title, 
                "function": func
            }
        )

    def run(self) -> None:
        # Drodown to select the page to run  
        page = st.sidebar.selectbox(
            'App Navigation', 
            self.pages, 
            format_func=lambda page: page.get('title', "")
        )

        # run the app function 
        page.get('function')()




def introduction_page():
    # Page title and welcome message
    st.markdown("# Welcome to the D|OPS Digital Work Experience Week!")
    st.write("We are excited to have you join us for a week of software development & DevOps learning & exploration.")

    # Introduction to the game "Find the Bug"
    st.markdown("## Game: Find the Bug")
    st.write("As part of this work experience week, we have created a game called 'Find the Bug' to help you learn how to find bugs in software.")

    # Description of the game and its purpose
    st.write("The game is built as a using Python & the Streamlit API and intentionally includes some bugs for you to find and fix. It's a fun and interactive way to gain practical experience in identifying and resolving software bugs.")

    # Instructions for playing the game
    st.markdown("### How to Play:")
    st.write("1. Navigate to the 'Image Generator' game page in the navigation menu on the left.")
    st.write("2. Explore the web application and try to find the bugs.")
    st.write("3. Once you identify a bug, think about the issue and how you would fix it.")
    st.write("4. Share your findings and proposed solutions with our mentors and fellow participants.")

    # Encouragement to learn and have fun
    st.markdown("### Let's Get Started!")
    st.write("Remember, the purpose of this week is to learn and have fun. Don't hesitate to ask questions and collaborate with your peers. Enjoy the journey of discovering the world of software development!")

# Render the introduction page
introduction_page()
