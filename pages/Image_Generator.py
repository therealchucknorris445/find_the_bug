

import os
import streamlit as st
import openai
import logging
from PIL import Image
import requests
from io import BytesIO

openai.api_key = os.environ.get("OPENAI_API_KEY")

# Configure logging
logging.basicConfig(level=logging.INFO)

# List to store generated image data
image_archive = []

# Dictionary for string replacements
string_replacements = {
    "red": "blue",
    "green": "yellow",
    "white": "black",
    "black": "white",
    "football": "basketball",
    "water": "mud",
    "fish": "frog",
    "bird": "worm"
}

def replace_strings(prompt):
    for string, replacement in string_replacements.items():
        prompt = prompt.replace(string, replacement)
    return prompt

def generate_image(prompt):
    try:
        prompt = replace_strings(prompt)  # Perform string replacements

        response = openai.Image.create(
            prompt=prompt,
            n=1,
            size="1024x1024"
        )

        image_url = response['data'][0]['url']
        return image_url
    except Exception as e:
        logging.error(f"API Error: {str(e)}")

    return None

def main():
    st.title("Image Generator AI")
    prompt = st.text_input("Enter text prompt here")
    if st.button("Generate Image"):
        if prompt:
            image_url = generate_image(prompt)
            if image_url:
                st.image(image_url, caption="Generated Image")

                # Download image
                response = requests.get(image_url)
                image = Image.open(BytesIO(response.content))
                image_filename = f"generated_image_{len(image_archive)}.jpg"
                image.save(image_filename, "JPEG")

                st.download_button(
                    label="Download Image",
                    data=open(image_filename, "rb"),
                    file_name=image_filename,
                    mime="image/jpeg"
                )

            else:
                st.warning("Failed to generate image. Please try again.")
        else:
            st.warning("Please enter a prompt.")

if __name__ == "__main__":
    main()

