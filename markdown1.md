# **Introduction to Software Development: Using a MultiPage Web App with Streamlit:**


In this article, we will explore a [Python](https://www.python.org/) script that demonstrates how to create a [multi-page web application](https://docs.streamlit.io/library/get-started/multipage-apps/create-a-multipage-app) using the [Streamlit API](https://docs.streamlit.io/), a user-friendly library for building data-driven web apps.

Prerequisites:

To follow along with this tutorial, to have Python installed on the laptop you’ve been provided. Additionally, we’ll need to install the Streamlit API. You can do this using the command `pip install streamlit`, or `pip3 install streamlit` for [Python3](https://www.python.org/download/releases/3.0/).

Understanding the Code:

Let's dive into the code and understand what it does:

1. Importing Streamlit:

- The script starts by [importing](https://docs.python.org/3/reference/import.html) the necessary library, Streamlit, which provides an intuitive interface for creating web applications.

2. Creating the MultiPage Class:

- The script defines a [class](https://realpython.com/lessons/classes-python/) called `MultiPage`, which acts as a container for our web application.

- The `__init__` method is the constructor that initializes the class and creates an empty list called `pages`. This list will store all the pages of our application.

3. Adding Pages:

- The `add_page` method allows us to add pages to our web application.

- Each page is defined by a title (a string) and a function that renders the content of that page.

- The method adds the page to the `pages` list as a dictionary, with the title and function as key-value pairs.

4. Running the Application:

- The `run` method is responsible for running the web application.

- It displays a dropdown menu in the sidebar, allowing the user to select the desired page.

- The selected page's corresponding function is then executed, rendering the content on the main area of the web app.

Conclusion:

In this article, we explored a Python script that showcases the basics of creating a multi-page web application using Streamlit. We discussed the code step by step, focusing on its purpose and functionality. By running the script, you can learn how to build interactive web applications with multiple pages. With further exploration and experimentation, you can expand your knowledge of web development and create more advanced applications. Have fun coding!
