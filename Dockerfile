FROM python:3.9-slim-buster

WORKDIR /app

COPY requirements.txt .

RUN pip3 install --no-cache-dir -r requirements.txt

# Copy the application files to the working directory
COPY . .

EXPOSE 8501

ENV OPENAI_API_KEY ${OPENAI_API_KEY}

CMD ["streamlit", "run", "HOME.py"]
